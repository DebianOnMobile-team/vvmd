/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2021, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <glib.h>
#include <glib/gprintf.h>

#include "phone-utils.h"

static void
test_number_decode (gconstpointer data)
{
  char *output;
  gboolean valid = FALSE;

  const char phone_test1[] = "12065550110";
  const char phone_test2[] = "2065550110";
  const char phone_test3[] = "(206) 555-0110";
  const char phone_test4[] = "206-555-0110";

  const char phone_output[] = "+12065550110";

  const char email_test1[] = "testemail@example.com";

  //Free Mobile FR Dest Number
  const char carrer_vvm_test1[] = "2050";

  //AT&T US
  const char carrer_vvm_test2[] = "94183567";
  //AT&T US: This is where the texts come from
  const char carrer_vvm_test6[] = "1000000000";

  //T-mobile US and Mint Mobile US
  const char carrer_vvm_test3[] = "127";

  //Ting US
  const char carrer_vvm_test4[] = "122";

  //VZW USA US
  const char carrer_vvm_test5[] = "900080006200";

  output = phone_utils_format_number_e164 (phone_test1, "US", FALSE);
  g_assert_cmpstr (output, ==, phone_output);

  output = phone_utils_format_number_e164 (phone_test2, "US", FALSE);
  g_assert_cmpstr (output, ==, phone_output);

  output = phone_utils_format_number_e164 (phone_test3, "US", FALSE);
  g_assert_cmpstr (output, ==, phone_output);

  output = phone_utils_format_number_e164 (phone_test4, "US", FALSE);
  g_assert_cmpstr (output, ==, phone_output);

  output = phone_utils_format_number_e164 (phone_output, "US", FALSE);
  g_assert_cmpstr (output, ==, phone_output);


  output = phone_utils_format_number_e164 (email_test1, "US", TRUE);
  g_assert_cmpstr (output, ==, email_test1);

  output = phone_utils_format_number_e164 (email_test1, "US", FALSE);
  g_assert_null (output);

  output = phone_utils_format_number_e164 (carrer_vvm_test1, "US", FALSE);
  g_assert_cmpstr (output, ==, carrer_vvm_test1);

  output = phone_utils_format_number_e164 (carrer_vvm_test2, "US", FALSE);
  g_assert_cmpstr (output, ==, carrer_vvm_test2);

  output = phone_utils_format_number_e164 (carrer_vvm_test3, "US", FALSE);
  g_assert_cmpstr (output, ==, carrer_vvm_test3);

  output = phone_utils_format_number_e164 (carrer_vvm_test4, "US", FALSE);
  g_assert_cmpstr (output, ==, carrer_vvm_test4);

  output = phone_utils_format_number_e164 (carrer_vvm_test5, "US", FALSE);
  g_assert_cmpstr (output, ==, carrer_vvm_test5);

  valid = phone_utils_simple_is_valid (phone_test1);
  g_assert (valid);
  valid = phone_utils_is_valid (phone_test1, "US");
  g_assert (valid);

  valid = phone_utils_simple_is_valid (phone_test2);
  g_assert (valid);
  valid = phone_utils_is_valid (phone_test2, "US");
  g_assert (valid);

  valid = phone_utils_simple_is_valid (phone_test3);
  g_assert (valid);
  valid = phone_utils_is_valid (phone_test3, "US");
  g_assert (valid);

  valid = phone_utils_simple_is_valid (phone_test4);
  g_assert (valid);
  valid = phone_utils_is_valid (phone_test4, "US");
  g_assert (valid);

  valid = phone_utils_simple_is_valid (email_test1);
  g_assert (!valid);
  valid = phone_utils_is_valid (email_test1, "US");
  g_assert (!valid);

  valid = phone_utils_simple_is_valid (carrer_vvm_test1);
  g_assert (valid);
  valid = phone_utils_is_valid (carrer_vvm_test1, "FR");
  g_assert (!valid);

  valid = phone_utils_simple_is_valid (carrer_vvm_test2);
  g_assert (valid);
  valid = phone_utils_is_valid (carrer_vvm_test2, "US");
  g_assert (!valid);

  valid = phone_utils_simple_is_valid (carrer_vvm_test3);
  g_assert (valid);
  valid = phone_utils_is_valid (carrer_vvm_test3, "US");
  g_assert (!valid);

  valid = phone_utils_simple_is_valid (carrer_vvm_test4);
  g_assert (valid);
  valid = phone_utils_is_valid (carrer_vvm_test4, "US");
  g_assert (!valid);

  valid = phone_utils_simple_is_valid (carrer_vvm_test5);
  g_assert (valid);
  valid = phone_utils_is_valid (carrer_vvm_test5, "US");
  g_assert (!valid);

  valid = phone_utils_simple_is_valid (carrer_vvm_test6);
  g_assert (valid);
  valid = phone_utils_is_valid (carrer_vvm_test6, "US");
  g_assert (!valid);
}

int
main (int    argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_data_func ("/vvmutil/Number Decode Test",
                        NULL, test_number_decode);


  return g_test_run ();
}
