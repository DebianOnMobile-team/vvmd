/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2022, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <glib.h>
#include <glib/gprintf.h>

#include "resolve.h"
#include "vvmutil.h"
#include "vvm3-activation.h"

#define SPGURL_TO_TEST_ONE "https://spg.vzw.com/newSelfProvisioning/SelfProvisioning"
#define BUTTONURL_TO_TEST_ONE "https://spg.vzw.com/newSelfProvisioning/bvvmprovision?sfo=XXXXX"

static const char FIRST_REQUEST_0[] = "./unit/vvm3_activation/first request.xml";
static const char SECOND_REQUEST_0[] = "./unit/vvm3_activation/second request.html";
static const char SECOND_REQUEST_1[] = "./unit/vvm3_activation/second request 1.html";

// Valid URLs
static const char url_0[] = "https://spg.vzw.com/newSelfProvisioning/SelfProvisioning";
static const char url_1[] = "https://spg.vzw.com:443/newSelfProvisioning/bvvmdecline";
static const char url_2[] = "http://spg.vzw.com/newSelfProvisioning/SelfProvisioning";
static const char url_3[] = "http://spg.vzw.com:443/newSelfProvisioning/bvvmdecline";
static const char url_4[] = "http://spg.dsfsdf.dgssfdds.vzw.com/newSelfProvisioning/SelfProvisioning";
static const char url_5[] = "http://spgrrfbnjt.ssxvcr.45td.vzw.com:443/newSelfProvisioning/bvvmdecline";
// Invalid Domain
static const char url_6[] = "http://spg.not_vzw.com/newSelfProvisioning/SelfProvisioning";
static const char url_7[] = "http://spg.not_vzw.com:443/newSelfProvisioning/bvvmdecline";
// Not http:// or https://
static const char url_8[] = "ftp://spg.vzw.com:443/newSelfProvisioning/bvvmdecline";
static const char url_9[] = "spg.vzw.com:443/newSelfProvisioning/bvvmdecline";
// Valid URL, Activate URL
static const char url_10[] = "https://vmg.vzw.com/VMGIMS/VMServices";
// Put .vzw.com not in domain
static const char url_11[] = "http://spg.example.com/.vzw.com/newSelfProvisioning/SelfProvisioning";
static const char url_12[] = "http://spg.example.com/.vzw.com:443/newSelfProvisioning/SelfProvisioning";
// .vzw.com is a subdommain, not root domain
static const char url_13[] = "https://vmg.vzw.com.example.com/VMGIMS/VMServices";

static void
append_file_to_g_string (const char *file_to_copy,
                         GString    *data_to_parse)
{
  unsigned int len;
  struct stat st;
  unsigned char *pdu;
  int fd;

  fd = open (file_to_copy, O_RDONLY);
  if (fd < 0)
    {
      g_printerr ("Failed to open path %s\n", file_to_copy);
      goto out;
    }

  if (fstat (fd, &st) < 0)
    {
      g_printerr ("Failed to stat %s\n", file_to_copy);
      close (fd);
      goto out;
    }

  len = st.st_size;

  pdu = mmap (NULL, len, PROT_READ, MAP_SHARED, fd, 0);
  if (!pdu || pdu == MAP_FAILED)
    {
      g_printerr ("Failed to mmap %s\n", file_to_copy);
      close (fd);
      goto out;
    }

  data_to_parse = g_string_append (data_to_parse, (char *) pdu);

  munmap (pdu, len);

  close (fd);
  return;

 out:
  g_assert (FALSE);
  return;
}

static void
test_vvm3 (gconstpointer data)
{
  g_autofree char *buttonUrl = NULL;
  g_autofree char *spgUrl = NULL;
  GString *data_to_parse;

  data_to_parse = g_string_new (NULL);

  append_file_to_g_string (FIRST_REQUEST_0, data_to_parse);

  spgUrl = vvm3_xml_find_spgUrl (data_to_parse);


  if (g_strcmp0 (spgUrl, SPGURL_TO_TEST_ONE) != 0)
    g_printerr ("Captured URL id %s not the same as original %s\n", spgUrl, SPGURL_TO_TEST_ONE);

  g_assert (g_strcmp0 (spgUrl, SPGURL_TO_TEST_ONE) == 0);

  g_string_free (data_to_parse, TRUE);
  data_to_parse = g_string_new (NULL);

  append_file_to_g_string (SECOND_REQUEST_0, data_to_parse);

  buttonUrl = vvm3_html_find_buttonUrl (data_to_parse);

  if (g_strcmp0 (buttonUrl, BUTTONURL_TO_TEST_ONE) != 0)
    g_printerr ("Captured URL id %s not the same as original %s\n", buttonUrl, BUTTONURL_TO_TEST_ONE);

  g_assert (g_strcmp0 (buttonUrl, BUTTONURL_TO_TEST_ONE) == 0);

  g_string_free (data_to_parse, TRUE);

  data_to_parse = g_string_new (NULL);

  append_file_to_g_string (SECOND_REQUEST_1, data_to_parse);

  buttonUrl = vvm3_html_find_buttonUrl (data_to_parse);

  if (g_strcmp0 (buttonUrl, BUTTONURL_TO_TEST_ONE) != 0)
    g_printerr ("Captured URL id %s not the same as original %s\n", buttonUrl, BUTTONURL_TO_TEST_ONE);

  g_assert (g_strcmp0 (buttonUrl, BUTTONURL_TO_TEST_ONE) == 0);

  g_string_free (data_to_parse, TRUE);

  g_assert (validate_vvm3_host (SPGURL_TO_TEST_ONE));
  g_assert (validate_vvm3_host (BUTTONURL_TO_TEST_ONE));
  g_assert (validate_vvm3_host (url_0));
  g_assert (validate_vvm3_host (url_1));
  g_assert (validate_vvm3_host (url_2));
  g_assert (validate_vvm3_host (url_3));
  g_assert (validate_vvm3_host (url_4));
  g_assert (validate_vvm3_host (url_5));
  g_assert (!validate_vvm3_host (url_6));
  g_assert (!validate_vvm3_host (url_7));
  g_assert (!validate_vvm3_host (url_8));
  g_assert (!validate_vvm3_host (url_9));
  g_assert (validate_vvm3_host (url_10));
  g_assert (!validate_vvm3_host (url_11));
  g_assert (!validate_vvm3_host (url_12));
  g_assert (!validate_vvm3_host (url_13));

  return;
}

int
main (int    argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_data_func ("/vvmutil/vvm3 Activation Test",
                        NULL, test_vvm3);


  return g_test_run ();
}
