/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2021, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <glib.h>
#include <glib/gprintf.h>

#include "vvmutil.h"

static void
test_decode (gconstpointer data)
{
  char *output;

  const char decode_0[] = "WFRRXlpTVVVfWTAwMDAw";         //000000000000000
  const char decode_1[] = "WVVQX1tSVFReWDExMTEx";         //111111111111111
  const char decode_2[] = "WlZTXFhRV1ddWzIyMjIy";         //222222222222222
  const char decode_3[] = "W1dSXVlQVlZcWjMzMzMz";         //333333333333333
  const char decode_4[] = "XFBVWl5XUVFbXTQ0NDQ0";         //444444444444444
  const char decode_5[] = "XVFUW19WUFBaXDU1NTU1";         //555555555555555
  const char decode_6[] = "XlJXWFxVU1NZXzY2NjY2";         //666666666666666
  const char decode_7[] = "X1NWWV1UUlJYXjc3Nzc3";         //777777777777777
  const char decode_8[] = "UFxZVlJbXV1XUTg4ODg4";         //888888888888888
  const char decode_9[] = "UV1YV1NaXFxWUDk5OTk5";         //999999999999999
  const char decode_007[] = "WFRRXlpTVQ==";               //0000000
  const char decode_008[] = "WFRRXlpTVVU=";               //00000000
  const char decode_009[] = "WFRRXlpTVVVf";               //000000000
  const char decode_010[] = "WFRRXlpTVVVfWQ==";           //0000000000
  const char decode_011[] = "WFRRXlpTVVVfWTA=";           //00000000000
  const char decode_012[] = "WFRRXlpTVVVfWTAw";           //000000000000
  const char decode_013[] = "WFRRXlpTVVVfWTAwMA==";       //0000000000000
  const char decode_014[] = "WFRRXlpTVVVfWTAwMDA=";       //00000000000000

  output = decode (decode_007);
  g_assert (g_strcmp0 (output, "0000000") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_008);
  g_assert (g_strcmp0 (output, "00000000") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_009);
  g_assert (g_strcmp0 (output, "000000000") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_010);
  g_assert (g_strcmp0 (output, "0000000000") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_011);
  g_assert (g_strcmp0 (output, "00000000000") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_012);
  g_assert (g_strcmp0 (output, "000000000000") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_013);
  g_assert (g_strcmp0 (output, "0000000000000") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_014);
  g_assert (g_strcmp0 (output, "00000000000000") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_0);
  g_assert (g_strcmp0 (output, "000000000000000") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_1);
  g_assert (g_strcmp0 (output, "111111111111111") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_2);
  g_assert (g_strcmp0 (output, "222222222222222") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_3);
  g_assert (g_strcmp0 (output, "333333333333333") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_4);
  g_assert (g_strcmp0 (output, "444444444444444") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_5);
  g_assert (g_strcmp0 (output, "555555555555555") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_6);
  g_assert (g_strcmp0 (output, "666666666666666") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_7);
  g_assert (g_strcmp0 (output, "777777777777777") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_8);
  g_assert (g_strcmp0 (output, "888888888888888") == 0);
  g_free (output);
  output = NULL;

  output = decode (decode_9);
  g_assert (g_strcmp0 (output, "999999999999999") == 0);
  g_free (output);
  output = NULL;
}

int
main (int    argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_data_func ("/vvmutil/Decode Test",
                        NULL, test_decode);


  return g_test_run ();
}
