/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2010-2011  Intel Corporation.
 *                2021, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <glib.h>
#include <glib/gprintf.h>

#include "vvmutil.h"

#define VVM_SHA1_UUID_LEN 20

struct vvm_test
{
  const char *pathname;
  const char *content_type;
  const char *to;
  const char *from;
  const char *date;
  const char *mime;
  const char *message_context;
};

static const char vvm_mbox_1[] = "./unit/Tmobile1Headers.txt";
static const char vvm_mbox_1_content_type[] = "multipart/mixed; boundary=\"_Part_922_1624281418\"";
static const char vvm_mbox_1_to[] = "VOICE=+12065550110@domain.com";
static const char vvm_mbox_1_from[] = "VOICE=+14325550110@tmo.com";
static const char vvm_mbox_1_date[] = "Mon, 21 Jun 2021 13:16:58 +0000";
static const char vvm_mbox_1_mime[] = "1.0";
static const char vvm_mbox_1_message_context[] = "voice-message";

static const struct vvm_test vvm_test_1 = {
  .pathname = vvm_mbox_1,
  .content_type = vvm_mbox_1_content_type,
  .to = vvm_mbox_1_to,
  .from = vvm_mbox_1_from,
  .date = vvm_mbox_1_date,
  .mime = vvm_mbox_1_mime,
  .message_context = vvm_mbox_1_message_context,
};

static const char vvm_mbox_2[] = "./unit/ATT1Headers.txt";
static const char vvm_mbox_2_content_type[] = "multipart/mixed; Boundary=\"============>>AnyPath 1624551264<<============\"";
static const char vvm_mbox_2_to[] = "\"2065550110\" <2065550110@crafpaalvml001.nsdeng.att.com>";
static const char vvm_mbox_2_from[] = "\"4325550110\" <4325550110@crafpaalvml001.nsdeng.att.com>";
static const char vvm_mbox_2_date[] = "Thu, 24 Jun 2021 12:14:24 -0400";
static const char vvm_mbox_2_mime[] = "1.0 (Voice 2.0)";
static const char vvm_mbox_2_message_context[] = "Voice-message";

static const struct vvm_test vvm_test_2 = {
  .pathname = vvm_mbox_2,
  .content_type = vvm_mbox_2_content_type,
  .to = vvm_mbox_2_to,
  .from = vvm_mbox_2_from,
  .date = vvm_mbox_2_date,
  .mime = vvm_mbox_2_mime,
  .message_context = vvm_mbox_2_message_context,
};

static const char vvm_mbox_3[] = "./unit/MintMobile1Headers.txt";
static const char vvm_mbox_3_content_type[] = "multipart/mixed; boundary=\"_Part_740_1626995070\"";
static const char vvm_mbox_3_to[] = "VOICE=4325550110@domain.com";
static const char vvm_mbox_3_from[] = "VOICE=2065550110@tmo.com";
static const char vvm_mbox_3_date[] = "Thu, 22 Jul 2021 23:04:30 +0000";
static const char vvm_mbox_3_mime[] = "1.0";
static const char vvm_mbox_3_message_context[] = "voice-message";

static const struct vvm_test vvm_test_3 = {
  .pathname = vvm_mbox_3,
  .content_type = vvm_mbox_3_content_type,
  .to = vvm_mbox_3_to,
  .from = vvm_mbox_3_from,
  .date = vvm_mbox_3_date,
  .mime = vvm_mbox_3_mime,
  .message_context = vvm_mbox_3_message_context,
};

static const char vvm_mbox_4[] = "./unit/VZWUSA1Headers.txt";
static const char vvm_mbox_4_content_type[] = "multipart/voice-message;boundary=\"----=_Part_45040777_18760913.1634822783904\"";
static const char vvm_mbox_4_to[] = "+15465557654@vzwazc.com";
static const char vvm_mbox_4_from[] = "+15235555432@vzwazc.com";
static const char vvm_mbox_4_date[] = "Thu, 21 Oct 2021 09:26:23 -0400 (EDT)";
static const char vvm_mbox_4_mime[] = "1.0 (Voice Version 2.0)";
static const char vvm_mbox_4_message_context[] = "voice-message";

static const struct vvm_test vvm_test_4 = {
  .pathname = vvm_mbox_4,
  .content_type = vvm_mbox_4_content_type,
  .to = vvm_mbox_4_to,
  .from = vvm_mbox_4_from,
  .date = vvm_mbox_4_date,
  .mime = vvm_mbox_4_mime,
  .message_context = vvm_mbox_4_message_context,
};

/*
 * Some carriers don't seem to actually capitalize their headers, which leads to bad things.
 * This is one example of such behavior.
 */
static const char vvm_mbox_5[] = "./unit/Frbouyguestelecomheaders.txt";
static const char vvm_mbox_5_content_type[] = "multipart/mixed; boundary=\"uT8a1N0e3E0r3CeNmIeVht\"";
static const char vvm_mbox_5_to[] = "<0267865545@bouyguestelecom.fr>";
static const char vvm_mbox_5_from[] = "<0236438476@bouyguestelecom.fr>";
static const char vvm_mbox_5_date[] = "Mon, 29 Jul 2024 16:01:57 +0200";
static const char vvm_mbox_5_mime[] = "1.0 (Voice Version 2.0)";
static const char vvm_mbox_5_message_context[] = "voice-message";

static const struct vvm_test vvm_test_5 = {
  .pathname = vvm_mbox_5,
  .content_type = vvm_mbox_5_content_type,
  .to = vvm_mbox_5_to,
  .from = vvm_mbox_5_from,
  .date = vvm_mbox_5_date,
  .mime = vvm_mbox_5_mime,
  .message_context = vvm_mbox_5_message_context,
};

static const char vvm_mbox_6[] = "./unit/orangefrHeaders.txt";
static const char vvm_mbox_6_content_type[] = "multipart/mixed;boundary=\"----=_Part_9352565_997656403.1737124168041\"";
static const char vvm_mbox_6_to[] = "VOICE=+NUMBERTO@dav.orange.fr";
static const char vvm_mbox_6_from[] = "VOICE=+NUMBERFROM@dav.orange.fr";
static const char vvm_mbox_6_date[] = "Fri, 17 Jan 2025 15:01:36 +0100 (CET)";
static const char vvm_mbox_6_mime[] = "1.0";
/*
 * This isn't a header in this one
 * TODO: This header is mandatory, but it doesn't seem like it does anything.
 */
static const char vvm_mbox_6_message_context[] = "";

static const struct vvm_test vvm_test_6 = {
  .pathname = vvm_mbox_6,
  .content_type = vvm_mbox_6_content_type,
  .to = vvm_mbox_6_to,
  .from = vvm_mbox_6_from,
  .date = vvm_mbox_6_date,
  .mime = vvm_mbox_6_mime,
  .message_context = vvm_mbox_6_message_context,
};

static void
test_decode_headers (gconstpointer data)
{
  const struct vvm_test *test = data;
  struct voicemail *vvm_msg;
  unsigned int len;
  char **tokens = NULL;
  struct stat st;
  unsigned char *pdu;
  int fd;

  vvm_msg = g_try_new0 (struct voicemail, 1);
  if (vvm_msg == NULL)
    {
      g_printerr ("Failed allocate message");
      return;
    }

  fd = open (test->pathname, O_RDONLY);
  if (fd < 0)
    {
      g_printerr ("Failed to open path %s\n", test->pathname);
      goto out;
    }

  if (fstat (fd, &st) < 0)
    {
      g_printerr ("Failed to stat %s\n", test->pathname);
      close (fd);
      goto out;
    }

  len = st.st_size;

  pdu = mmap (NULL, len, PROT_READ, MAP_SHARED, fd, 0);
  if (!pdu || pdu == MAP_FAILED)
    {
      g_printerr ("Failed to mmap %s\n", test->pathname);
      close (fd);
      goto out;
    }

  tokens = g_strsplit_set ((char *) pdu, "\r\n", -1);
  vvm_util_decode_vvm_headers (vvm_msg, tokens);
  g_strfreev (tokens);

  g_assert_cmpstr (test->content_type, ==, vvm_msg->content_type);
  g_assert_cmpstr (test->date, ==, vvm_msg->message_date);
  g_assert_cmpstr (test->from, ==, vvm_msg->message_sender);
  g_assert_cmpstr (test->to, ==, vvm_msg->to);
  if (vvm_msg->message_context)
    g_assert_cmpstr (test->message_context, ==, vvm_msg->message_context);
  g_assert_cmpstr (test->mime, ==, vvm_msg->mime_version);

  munmap (pdu, len);

  close (fd);

  vvm_util_delete_vvm_message (vvm_msg);
  return;

 out:
  vvm_util_delete_vvm_message (vvm_msg);
  g_assert (FALSE);
}

int
main (int    argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_data_func ("/vvmutil/Decode T-Mobile USA 1 headers",
                        &vvm_test_1, test_decode_headers);

  g_test_add_data_func ("/vvmutil/Decode AT&T USA 1 headers",
                        &vvm_test_2, test_decode_headers);

  g_test_add_data_func ("/vvmutil/Decode Mint Mobile 1 headers",
                        &vvm_test_3, test_decode_headers);

  g_test_add_data_func ("/vvmutil/Decode VZW USA 1 headers",
                        &vvm_test_4, test_decode_headers);

  g_test_add_data_func ("/vvmutil/Decode Fr bouygues telecom headers",
                        &vvm_test_5, test_decode_headers);

  g_test_add_data_func ("/vvmutil/Decode Fr orange telecom headers",
                        &vvm_test_6, test_decode_headers);

  return g_test_run ();
}
