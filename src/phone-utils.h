/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2012, 2013 Intel Corporation
 *                2021, Chris Talbot
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 3 or later as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

gchar       *phone_utils_format_number_e164   (const char *number,
                                               const char *country_code,
                                               gboolean    return_original_number);
gboolean     phone_utils_is_valid             (const char *number,
                                               const char *country_code);
gboolean     phone_utils_simple_is_valid      (const char *number);
G_END_DECLS
