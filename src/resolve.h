/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2024, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <gio/gio.h>
#include <curl/curl.h>

char *vvm_resolve_resolve_host (const char *host,
                                const char *mailbox_interface,
                                const char *resolvers_ipv4_csv,
                                const char *resolvers_ipv6_csv);

int   vvm_resolve_ip_version (const char *src);
