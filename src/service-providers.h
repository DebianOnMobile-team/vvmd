/*
 *
 *  Multimedia Messaging Service Daemon - The Next Generation
 *
 *  Copyright (C) 2009, Novell, Inc.
 *        Author: Tambet Ingo (tambet@gmail.com).
 *                2009-2019, Red Hat, Inc.
 *                2012, Lanedo GmbH
 *                2021, Chris Talbot <chris@talbothome.com>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

typedef void (*VvmdServiceProvidersCallback)(const char *carrier,
                                             const char *vvm_std,
                                             const char *dest_num,
                                             const char *carrier_prefix,
                                             GError     *error,
                                             gpointer    user_data);

void  vvmd_service_providers_find_settings (const char                  *service_providers,
                                            const char                  *mccmnc,
                                            VvmdServiceProvidersCallback callback,
                                            gpointer                     user_data);
